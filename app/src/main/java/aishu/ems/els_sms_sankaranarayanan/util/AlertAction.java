package aishu.ems.els_sms_sankaranarayanan.util;

/**
 * Created by ygs on 14/05/17.
 */

public interface AlertAction {

    public void permissionGranted(int type);
    public void permissionDenied();
}
