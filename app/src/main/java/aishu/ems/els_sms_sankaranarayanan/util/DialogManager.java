package aishu.ems.els_sms_sankaranarayanan.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import aishu.ems.els_sms_sankaranarayanan.R;

/**
 * Created by ygs on 14/05/17.
 */

public class DialogManager {

    static Dialog alertOptionDialog;

    public static Dialog showDialog(final Activity context, String message,
                                    String buttonText, final Class<?> move) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);

        builder.setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(buttonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (move == null) {
                                    alertOptionDialog.cancel();
                                } else {
                                    Intent cacheIntent = new Intent(context,
                                            move);

                                    context.startActivity(cacheIntent);

                                    context.finish();
                                }
                            }
                        });

        alertOptionDialog = builder.show();

        alertOptionDialog.show();
        return alertOptionDialog;
    }

    public static Dialog showMessageDialog(final Activity context,
                                           String message, String buttonText) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(buttonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                alertOptionDialog.cancel();

                            }
                        });

        alertOptionDialog = builder.show();

        alertOptionDialog.show();

        return alertOptionDialog;
    }

    public static void showToast(Context context, String message) {

        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static Dialog showOptionDialog(final Activity context,
                                          String message, String buttonText, String buttonText2,
                                          final Class<?> move, final int animFrom, final int animTo) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(buttonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (move == null) {
                                    alertOptionDialog.cancel();
                                } else {
                                    Intent cacheIntent = new Intent(context,
                                            move);

                                    context.startActivity(cacheIntent);
                                    // Setting screen transition
                                    context.overridePendingTransition(animFrom,
                                            animTo);
                                    context.finish();
                                }
                            }
                        })
                .setNegativeButton(buttonText2,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                alertOptionDialog.cancel();
                            }
                        });

        alertOptionDialog = builder.show();

        alertOptionDialog.show();
        return alertOptionDialog;
    }

    public static Dialog showOptionDialogWithOutImplementation(final Activity context,
                                                               String message, String buttonText, String buttonText2,
                                                               final int type, final AlertAction action) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(buttonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                               alertOptionDialog.cancel();
                                action.permissionGranted(type);
                            }
                        })
                .setNegativeButton(buttonText2,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                alertOptionDialog.cancel();
                                action.permissionDenied();
                            }
                        });
        alertOptionDialog = builder.show();

        alertOptionDialog.show();
        return alertOptionDialog;
    }
    public static ProgressDialog getProgressDialog(Activity activity,
                                                   int message) {
        ProgressDialog dialog = new ProgressDialog(activity);

        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle(message);

        return dialog;
    }


}

