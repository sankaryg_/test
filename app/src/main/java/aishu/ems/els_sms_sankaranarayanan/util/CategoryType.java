package aishu.ems.els_sms_sankaranarayanan.util;

/**
 * Created by ygs on 14/05/17.
 */

public enum CategoryType {
    TRANSACTION,
    TRAVELS,
    NON_TRANSACTION
}
