package aishu.ems.els_sms_sankaranarayanan.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ygs on 12/05/17.
 */

public class Constants {

    public static final String READ_SMS = "ELE.READ_SMS";

    public static final String mPreference = "ELE";

    public static int STRING_PREFERENCE = 1;
    public static int INT_PREFERENCE = 2;
    public static int BOOLEAN_PREFERENCE = 3;

    public static void storeValuetoPreference( Context mActivity,int preference,
                                        String key, Object value) {
        SharedPreferences sharedPreference = mActivity.getSharedPreferences(
                Constants.mPreference, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreference.edit();
        if (preference == STRING_PREFERENCE) {
            edit.putString(key, (String) value);
        }

        if (preference == INT_PREFERENCE) {
            edit.putInt(key, (Integer) value);
        }

        if (preference == BOOLEAN_PREFERENCE) {
            edit.putBoolean(key, (Boolean) value);
        }

        edit.commit();

    }

    public static Object getValueFromPreference(
            Context mActivity,int preference, String key) {
        SharedPreferences sharedPreference = mActivity.getSharedPreferences(
                Constants.mPreference, Context.MODE_PRIVATE);

        if (preference == STRING_PREFERENCE) {
            return (Object) sharedPreference.getString(key, "0");
        }

        if (preference == INT_PREFERENCE) {
            return (Object) sharedPreference.getInt(key, 0);
        }

        if (preference == BOOLEAN_PREFERENCE) {
            return (Object) sharedPreference.getBoolean(key, false);
        }

        return null;

    }
}
