package aishu.ems.els_sms_sankaranarayanan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import aishu.ems.els_sms_sankaranarayanan.R;
import aishu.ems.els_sms_sankaranarayanan.entities.Message;
import aishu.ems.els_sms_sankaranarayanan.entities.ParseSms;
import aishu.ems.els_sms_sankaranarayanan.util.CategoryType;

/**
 * Created by ygs on 14/05/17.
 */

public class SmsAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<Message> mList;
    private Context mContext;
    private CategoryType mCategoryType;

    public SmsAdapter(Context context, ArrayList<Message> list, CategoryType categoryType){
        mContext = context;
        mList = list;
        mCategoryType = categoryType;

    }

    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() :0);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewHolder mainHolder = (ViewHolder) holder;
        if (mCategoryType == CategoryType.TRANSACTION){
            String mBankName = ParseSms.getCardName(mList.get(position).getBody());
            mainHolder.mCardBankName.setText( mBankName != null ? mBankName: "" );
            String mMoney = ParseSms.getTransactionMoney(mList.get(position).getBody());
            String str = null,str1 = null;
            if (mMoney != null && mMoney.contains("_")){
               str = mMoney.split("_")[0];
               str1 = mMoney.split("_")[1];
            }
            mainHolder.mCardAmount.setText( str != null ? str: "" );
            String mMerchant = ParseSms.getMerchantName(mList.get(position).getBody());
            mainHolder.mCardMarchantName.setText( mMerchant != null ? mMerchant: "Online" );
            mainHolder.mCardTransactionType.setText(str1 != null ? str1: "");
        }
        mainHolder.mCardTitle.setText(mList.get(position).getBody());

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                R.layout.item_row, parent, false);
        ViewHolder mainHolder = new ViewHolder(mainGroup,mCategoryType) {
            @Override
            public String toString() {
                return super.toString();
            }
        };



        return mainHolder;
    }
}
