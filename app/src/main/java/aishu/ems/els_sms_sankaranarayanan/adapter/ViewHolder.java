package aishu.ems.els_sms_sankaranarayanan.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import aishu.ems.els_sms_sankaranarayanan.R;
import aishu.ems.els_sms_sankaranarayanan.util.CategoryType;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ygs on 14/05/17.
 */

public abstract class ViewHolder extends RecyclerView.ViewHolder{

    CategoryType mCategoryType;

    @BindView(R.id.cardTitle)
    TextView mCardTitle;

    @BindView(R.id.transactionDetails)
    LinearLayout mTransactionDetails;
    @BindView(R.id.bankName)
    TextView mCardBankName;
    @BindView(R.id.amountData)
    TextView mCardAmount;

    @BindView(R.id.transactionPlaceDetails)
    LinearLayout mTransactionPlaceDetails;
    @BindView(R.id.merchantName)
    TextView mCardMarchantName;
    @BindView(R.id.typeTransaction)
    TextView mCardTransactionType;

    public ViewHolder(View view){
        super(view);
        ButterKnife.bind(this,view);
    }

    public ViewHolder(View view, CategoryType categoryType){
        super(view);
        mCategoryType = categoryType;
        ButterKnife.bind(this,view);
        if (mCategoryType == CategoryType.TRANSACTION){
            mTransactionDetails.setVisibility(View.VISIBLE);
            mTransactionPlaceDetails.setVisibility(View.VISIBLE);
        }else {
            mTransactionDetails.setVisibility(View.GONE);
            mTransactionPlaceDetails.setVisibility(View.GONE);
        }
    }
}
