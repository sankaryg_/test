package aishu.ems.els_sms_sankaranarayanan.entities;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ygs on 14/05/17.
 */

public class ParseSms {


    public static String getMerchantName(String bodyMsg){
        String mMerchantName = null;
        try{
            Pattern regEx = Pattern.compile("(?i)(?:\\sat\\s|in\\*)([A-Za-z0-9]*\\s?-?\\s?[A-Za-z0-9]*\\s?-?\\.?)");// || (?i)(?:\\sat\\s|in\\*)([A-Za-z0-9]*\\s?-?\\s?[A-Za-z0-9]*\\s?-?\\.?)");
            // Find instance of pattern matches
            Matcher m = regEx.matcher(bodyMsg.toUpperCase());
            if(m.find()){
                mMerchantName = m.group();
                mMerchantName = mMerchantName.replaceAll("^\\s+|\\s+$", "");//trim from start and end
                mMerchantName = mMerchantName.replace("Info.","");
            }
        }catch(Exception e){
            mMerchantName = null;
        }
        return mMerchantName;
    }

    public static String getTransactionMoney(String bodyMsg){
        String mTransactionMoney = null;
        Pattern regEx
                = Pattern.compile("(?i)(?:(?:RS|INR|MRP)\\.?\\s?)(\\d+(:?\\,\\d+)?(\\,\\d+)?(\\.\\d{1,2})?)");
        // Find instance of pattern matches
        Matcher m = regEx.matcher(bodyMsg.toUpperCase());
        if (m.find()) {
            try {
                String amount = m.group();//(m.group(0).replaceAll("inr", ""));
                amount = amount.replaceAll("rs", "");
                amount = amount.replaceAll("inr", "");
                amount = amount.replaceAll(" ", "");
                amount = amount.replaceAll(",", "");
                mTransactionMoney = amount;
                if (bodyMsg.contains("debited") ||
                        bodyMsg.contains("purchasing") || bodyMsg.contains("purchase") || bodyMsg.contains("dr")) {
                    //smsDto.setTransactionType("0");
                    mTransactionMoney += "_-";
                } else if (bodyMsg.contains("credited") || bodyMsg.contains("cr")) {
                    //smsDto.setTransactionType("1");
                    mTransactionMoney += "_+";
                }
                //smsDto.setParsed("1");
                //if (!Character.isDigit(smsDto.getSenderid().charAt(0)))
                //    resSms.add(smsDto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Log.e("No_matchedValue ", "No_matchedValue ");
        }
        return mTransactionMoney;
    }

    public static String getCardName(String bodyMsg){
        String mCardName = null;
        try{
            Pattern regEx = Pattern.compile("(?i)(?:\\smade on|ur|made a\\s|in\\*)([A-Za-z]*\\s?-?\\s[A-Za-z]*\\s?-?\\s[A-Za-z]*\\s?-?)");
            // Find instance of pattern matches
            Matcher m = regEx.matcher(bodyMsg.toUpperCase());
            if(m.find()){
                mCardName = m.group();
                //mCardName = mCardName.replaceAll("^\\s+|\\s+$", "");//trim from start and end
                //mCardName = mCardName.replace("Info.","");
            }
        }catch(Exception e){
            mCardName = null;
        }
        return mCardName;
    }

}
