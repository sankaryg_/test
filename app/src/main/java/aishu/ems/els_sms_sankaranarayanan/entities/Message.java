package aishu.ems.els_sms_sankaranarayanan.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import aishu.ems.els_sms_sankaranarayanan.util.CategoryType;

/**
 * Created by ygs on 12/05/17.
 */

public class Message implements Parcelable {

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    private String senderId;
    private int person;
    private String body;
    private long date;
    private int type;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public int getPerson() {
        return person;
    }

    public void setPerson(int person) {
        this.person = person;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(senderId);
        parcel.writeInt(person);
        parcel.writeString(body);
        parcel.writeLong(date);
        parcel.writeInt(type);
    }

    public Message(Parcel in){
        senderId = in.readString();
        person = in.readInt();
        body = in.readString();
        date = in.readLong();
        type = in.readInt();
    }

    public Message(){

    }
    @Override
    public int describeContents() {
        return 0;
    }
}
