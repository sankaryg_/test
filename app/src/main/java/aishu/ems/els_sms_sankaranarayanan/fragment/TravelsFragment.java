package aishu.ems.els_sms_sankaranarayanan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import aishu.ems.els_sms_sankaranarayanan.R;
import aishu.ems.els_sms_sankaranarayanan.adapter.SmsAdapter;
import aishu.ems.els_sms_sankaranarayanan.entities.Message;
import aishu.ems.els_sms_sankaranarayanan.util.CategoryType;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ygs on 14/05/17.
 */

public class TravelsFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private ArrayList<Message> mMessageList;

    private View mView;
    public TravelsFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        mMessageList = bundle.getParcelableArrayList("list");
        mView = inflater.inflate(R.layout.fragment, container, false);
        ButterKnife.bind(this,mView);
        setRecyclerView();
        return mView;
    }

    private void setRecyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView
                .setLayoutManager(new LinearLayoutManager(getActivity()));//Linear Items
        if (mMessageList != null) {
            SmsAdapter adapter = new SmsAdapter(getActivity(), mMessageList, CategoryType.TRAVELS);
            mRecyclerView.setAdapter(adapter);// set adapter on recyclerview
        }
    }
}
