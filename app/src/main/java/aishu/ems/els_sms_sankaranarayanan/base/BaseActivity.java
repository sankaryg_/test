package aishu.ems.els_sms_sankaranarayanan.base;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import aishu.ems.els_sms_sankaranarayanan.MainActivity;
import aishu.ems.els_sms_sankaranarayanan.entities.Message;
import aishu.ems.els_sms_sankaranarayanan.util.AlertAction;
import aishu.ems.els_sms_sankaranarayanan.util.CategoryType;
import aishu.ems.els_sms_sankaranarayanan.util.Constants;
import aishu.ems.els_sms_sankaranarayanan.util.DialogManager;

/**
 * Created by ygs on 12/05/17.
 */

public class BaseActivity extends AppCompatActivity {

    protected static final int READ_SMS_PERMISSION_CONSTANT = 100;
    protected static final int RECEIVE_SMS_PERMISSION_CONSTANT = 101;
    protected static final int REQUEST_PERMISSION_SETTING = 102;

    protected ArrayList<Message> mTransactional;
    protected ArrayList<Message> mTravels;
    protected ArrayList<Message> mNonTransactional;

    private LocalBroadcastManager localBroadcastManager;

    private final BroadcastReceiver mSmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        //loadAllSms();

        localBroadcastManager.registerReceiver(mSmsReceiver,filterList());
    }

    public IntentFilter filterList(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.READ_SMS);
        return intentFilter;
    }

    @Override
    protected void onStop() {
        localBroadcastManager.unregisterReceiver(mSmsReceiver);
        super.onStop();
    }

    final String SMS_URI_INBOX = "content://sms/inbox";



    public ArrayList<Message> loadAllSms(){
        ArrayList<Message> smsMessageArrayList = null;
        mTransactional = new ArrayList<>();
        mNonTransactional = new ArrayList<>();
        mTravels = new ArrayList<>();
        try {
           Uri uri = Uri.parse(SMS_URI_INBOX);
           String[] projection = new String[]{"_id","address","person","body","date","type"};
           Cursor cursor = getContentResolver().query(uri,projection,null,null,"date desc");
           if (cursor.moveToFirst()){
               smsMessageArrayList = new ArrayList<>();
               int mAddress = cursor.getColumnIndex("address");
               int mPerson = cursor.getColumnIndex("person");
               int mBody = cursor.getColumnIndex("body");
               int mDate = cursor.getColumnIndex("date");
               int mType = cursor.getColumnIndex("type");
               do {
                   Message message = new Message();
                   String strAddress = cursor.getString(mAddress);
                   int intPerson = cursor.getInt(mPerson);
                   String strbody = cursor.getString(mBody);
                   long longDate = cursor.getLong(mDate);
                   int int_Type = cursor.getInt(mType);
                   message.setSenderId(strAddress);
                   message.setPerson(intPerson);
                   message.setBody(strbody);
                   message.setDate(longDate);
                   message.setType(int_Type);
                   strAddress = strAddress.toUpperCase();
                   if (strAddress.matches("(?i)[A-Z]{2}-?[!,A-Z,0-9]{1,8}\\s*") || strAddress.matches("(?i)[0-9]{1,7}\\s*")) {
                       String[] names = strAddress.split("-");
                       if (names.length == 2) {
                           if(names[1].contains("HDFC")||names[1].contains("ICICI")||names[1].contains("SBI") || strbody.contains("NEFT") || strbody.contains("IMPS") ){
                               //message.setCategory(CategoryType.TRANSACTION);
                               mTransactional.add(message);
                           }else if(names[1].contains("OLA")){
                               //message.setCategory(CategoryType.TRAVELS);
                                mTravels.add(message);
                           }else {
                               //message.setCategory(CategoryType.NON_TRANSACTION);
                               mNonTransactional.add(message);
                           }
                       }else {
                           //message.setCategory(CategoryType.NON_TRANSACTION);
                           mNonTransactional.add(message);
                       }
                   }
                   /*Pattern regEx =
                           Pattern.compile("[a-zA-Z0-9]{2}-[a-zA-Z0-9]{6}");
                   Matcher m = regEx.matcher(strAddress);
                   if (m.find()) {
                       try {
                           String phoneNumber = m.group(0);
                           Long date = longDate;

                           Log.e("SmsReceiver Mine", "senderNum: " + phoneNumber + "; message: " + message);

                       } catch (Exception e) {
                           e.printStackTrace();
                       }
                   } else {
                       Log.e("Mismatch", "Mismatch value");
                   }*/
                   smsMessageArrayList.add(message);
               }while (cursor.moveToNext());
               if (!cursor.isClosed()) {
                   cursor.close();
                   cursor = null;
               }
           }else {
               smsMessageArrayList = null;
           }
       } catch (SQLiteException ex){

       }
       return smsMessageArrayList;
    }


    public void requestPermission(AlertAction alertAction){
        if (ActivityCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_SMS)) {
                //Show Information about why you need the permission
                DialogManager.showOptionDialogWithOutImplementation(BaseActivity.this,"This app needs sms read permission.","GRANT","CANCEL",READ_SMS_PERMISSION_CONSTANT,alertAction
                        );
            } else if ((Boolean)Constants.getValueFromPreference(BaseActivity.this,Constants.BOOLEAN_PREFERENCE,Manifest.permission.READ_SMS)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                DialogManager.showOptionDialogWithOutImplementation(BaseActivity.this,"This app needs sms read permission.","GRANT","CANCEL",REQUEST_PERMISSION_SETTING,alertAction
                );
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.READ_SMS}, READ_SMS_PERMISSION_CONSTANT);
            }

            Constants.storeValuetoPreference(BaseActivity.this,Constants.BOOLEAN_PREFERENCE,Manifest.permission.READ_SMS,true);

        } else {
            //You already have the permission, just go ahead.
            loadAllSms();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_SMS_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                loadAllSms();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_SMS)) {
                    //Show Information about why you need the permission
                    DialogManager.showOptionDialogWithOutImplementation(BaseActivity.this,"This app needs sms read permission.","GRANT","CANCEL",READ_SMS_PERMISSION_CONSTANT,((MainActivity)this).alertAction
                    );
                } else {
                    Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
