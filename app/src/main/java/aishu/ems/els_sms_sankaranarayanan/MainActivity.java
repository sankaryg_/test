package aishu.ems.els_sms_sankaranarayanan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

import aishu.ems.els_sms_sankaranarayanan.adapter.ViewPagerAdapter;
import aishu.ems.els_sms_sankaranarayanan.base.BaseActivity;
import aishu.ems.els_sms_sankaranarayanan.entities.Message;
import aishu.ems.els_sms_sankaranarayanan.fragment.MessageFragment;
import aishu.ems.els_sms_sankaranarayanan.fragment.TransactionFragment;
import aishu.ems.els_sms_sankaranarayanan.fragment.TravelsFragment;
import aishu.ems.els_sms_sankaranarayanan.util.AlertAction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnPageChange;

public class MainActivity extends BaseActivity implements AlertAction{

    public AlertAction alertAction;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    @BindView(R.id.buttonPanel)
    LinearLayout mButtonLayout;
    @BindView(R.id.btn1)
    Button mButton1;
    @BindView(R.id.btn2)
    Button mButton2;
    @BindView(R.id.btn3)
    Button mButton3;
    @BindView(R.id.btn4)
    Button mButton4;

    ArrayList<Message> mMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        alertAction = this;
        setSupportActionBar(mToolbar);
        if (Build.VERSION.SDK_INT>=23) {
            requestPermission(this);
        }else {
            mMessages = loadAllSms();
        }
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
    }


    //Setting View Pager
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        TransactionFragment mTransactionFragment = new TransactionFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("list",mTransactional);
        mTransactionFragment.setArguments(bundle);
        adapter.addFrag(mTransactionFragment, "TRANSANCTIONAL");
        TravelsFragment mTravelsFragment = new TravelsFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putParcelableArrayList("list",mTravels);
        mTravelsFragment.setArguments(bundle1);
        adapter.addFrag(mTravelsFragment, "TRAVELS");
        MessageFragment mMessageFragment = new MessageFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putParcelableArrayList("list",mNonTransactional);
        mMessageFragment.setArguments(bundle2);
        adapter.addFrag(mMessageFragment, "NON TRANSACTIONAL");
        viewPager.setAdapter(adapter);
        onPageSelected(0);
    }


    @OnPageChange(value = R.id.viewPager,callback = OnPageChange.Callback.PAGE_SELECTED)
    public void onPageSelected(int pos){
        mButtonLayout.setVisibility(View.VISIBLE);
        switch (pos){
            case 0:
                mButton1.setText(R.string.credit);
                mButton2.setText(R.string.debit);
                mButton3.setText(R.string.loan);
                mButton4.setText(R.string.msg);
                break;
            case 1:
                mButton1.setText(R.string.confirm);
                mButton2.setText(R.string.cancel);
                mButton3.setText(R.string.arrival);
                mButton4.setText(R.string.bill);
                break;
            default:
                mButtonLayout.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void permissionGranted(int type) {
        if (type == REQUEST_PERMISSION_SETTING){
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
        }else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_SMS}, type);
        }
    }

    @Override
    public void permissionDenied() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
               mMessages = loadAllSms();
               setupViewPager(mViewPager);
               mTabLayout.setupWithViewPager(mViewPager);
            }
        }
    }
}
